from rest_framework import generics

from . import models, serializers


# Create your views here.
class AnalyzeHost(generics.ListCreateAPIView):
    http_method_names = ['post']
    queryset = models.Host.objects.all()
    serializer_class = serializers.HostSerializer
