import uuid

from django.contrib.postgres.fields import ArrayField
from django.db import models

from api import constants


# Create your models here.
class Host(models.Model):
    id = models.fields.UUIDField(default=uuid.uuid4(), primary_key=True, unique=True, editable=False)
    assessment_token = models.fields.UUIDField()
    url = models.fields.URLField()
    endpoint_ip_address = models.fields.GenericIPAddressField(null=True)
    done_controllers = ArrayField(models.fields.UUIDField(), default=list)
    state = models.fields.CharField(max_length=50, default=constants.PENDING_STATUS)
    start_time = models.fields.DateTimeField()
    end_time = models.fields.DateTimeField(null=True)

    def complete_control(self, control_id):
        self.done_controllers.append(control_id)
        self.save()

    def finish_host_sampling(self):
        self.state = constants.WAITING_FOR_APPROVAL_STATUS
        self.save()
