from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = (
    url(r'(?i)AnalyzeHost/', views.AnalyzeHost.as_view(), name="analyze_host"),
)

urlpatterns = format_suffix_patterns(urlpatterns)
