import logging.config

import sentry_sdk
from django.conf import settings
from sentry_sdk.integrations.django import DjangoIntegration
from . import constants

logger = logging.getLogger(constants.LOGGER_NAME)
default_app_config = 'api.apps.ApiConfig'

sentry_sdk.init(
    dsn=constants.SENTRY_DSN,
    integrations=[DjangoIntegration()]
)

if settings.LOGGER_CONFIG:
    LOGGING_CONFIG = None
    logging.config.dictConfig(settings.LOGGER_CONFIG)
