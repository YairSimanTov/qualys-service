from . import core, serializers, logger


class StartUpMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        logger.debug('setting up scheduler')
        core.default_schedule_manager.run_continuously()
        logger.debug('setting up queue')
        core.default_jobs_queue.setup_queue()
        logger.debug('looking for not finished hosts samples')
        hosts = serializers.get_undone_host_samples()
        if hosts:
            logger.debug('starting to sample uncompleted samples')
            serializers.sample_hosts(*hosts)

    def __call__(self, request):
        return self.get_response(request)
