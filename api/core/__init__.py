from . import core_models, scheduler
from .. import constants

default_schedule_manager = scheduler.ScheduleManager(constants.SAMPLE_INTERVAL_IN_SECONDS)
default_jobs_queue = core_models.JobsQueue(default_schedule_manager)
