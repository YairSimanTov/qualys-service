from .. import constants, logger


class Step:
    def __init__(self, action):
        self.state = constants.PENDING_STATUS
        self.action = action

    def complete(self):
        self.state = constants.WAITING_FOR_APPROVAL_STATUS

    def is_completed(self):
        return self.state == constants.WAITING_FOR_APPROVAL_STATUS


class Control:
    def __init__(self, id, from_step, field_getter):
        self.field_getter = field_getter
        self.from_step = from_step
        self.id = id


class JobBundle:
    def __init__(self, host, *jobs):
        self.host = host
        self.jobs = jobs


class Queue:
    def __init__(self):
        self.items = []

    def is_empty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0, item)

    def dequeue(self):
        return self.items.pop()

    def remove_from_queue(self, item_to_remove=None):
        return [self.items.remove(item) for item in self.items if item == item_to_remove]

    @property
    def size(self):
        return len(self.items)


class JobsQueue:
    def __init__(self, scheduler, max_running_jobs=constants.MAX_RUNNING_JOBS, *jobs_bundles):
        self._queue = Queue()
        self._ready = False
        self.running_bundles = []
        self.max_running_jobs = max_running_jobs
        self.scheduler = scheduler
        [self.enqueue(bundle) for bundle in jobs_bundles]

    def setup_queue(self):
        self.scheduler.schedule_job(self._queue_runner)
        self._ready = True

    def enqueue(self, job_bundle):
        self._queue.enqueue(job_bundle)

    def stop_running_bundles(self, host):
        bundles_to_stop = [bundle for bundle in self.running_bundles if bundle.host == host]
        for bundle in bundles_to_stop:
            [job.stop() for job in bundle.jobs]
        [self.running_bundles.remove(bundle) for bundle in bundles_to_stop]

    def cancel_pending_bundles(self, host):
        bundles_to_stop = [bundle for bundle in self._queue.items if bundle.host == host]
        [self._queue.remove_from_queue(bundle) for bundle in bundles_to_stop]

    def remove_completed_bundles(self):
        [self.running_bundles.remove(bundle) for bundle in self.running_bundles if
         all(map(lambda job: not job.is_running, bundle.jobs))]

    def _queue_runner(self, done_callback):
        if not self._ready:
            logger.error('GLOBAL - Could not start, queue is not ready')
            return
        self.remove_completed_bundles()
        while self._queue.size and len(self.running_bundles) < self.max_running_jobs:
            bundle = self._queue.dequeue()
            [job.start() for job in bundle.jobs]
            self.running_bundles.append(bundle)
