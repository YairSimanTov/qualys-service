import json


def decode_bit_to_json(bit_array):
    return json.loads(bit_array.decode('utf8').replace("'", '"'))


def basic_field_getter(depth_fields=None, value_logic=lambda x: x):
    if depth_fields is None:
        depth_fields = []

    def wrapper(value):
        for depth_field in depth_fields:
            if depth_field in value:
                value = value[depth_field]
            else:
                return
        return value_logic(value)

    return wrapper


def number_bit_getter(position):
    def wrapper(value):
        return value & position

    return wrapper
