class NoEndpointsException(Exception):
    pass


class UnbegunException(Exception):
    pass


class AlreadyRunningException(Exception):
    pass


class RequestedAPIFailedException(Exception):
    pass


allowed_exceptions = [NoEndpointsException,
                      UnbegunException,
                      AlreadyRunningException,
                      RequestedAPIFailedException]
