import traceback

import requests
from django.conf import settings

from . import utils, default_schedule_manager, exceptions, core_models
from .. import constants, logger


def try_(should_raise=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                func(*args, **kwargs)
            except Exception as e:
                if not any(isinstance(e, exception) for exception in exceptions.allowed_exceptions):
                    logger.fatal(type(e).__name__)
                    traceback.print_exc()
                    if should_raise:
                        raise e
                logger.debug('Failed - {}'.format(type(e).__name__))

        return wrapper

    return decorator


class JobBase:
    is_running = False
    _job = None

    def __init__(self, schedule_manager):
        self._schedule_manager = schedule_manager

    def start(self):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError


class SimpleJob(JobBase):
    def __init__(self, action, *args, schedule_manager=default_schedule_manager):
        super().__init__(schedule_manager)
        self.action = action
        self.action_args = args

    def start(self):
        if not self.is_running:
            self._job = self._schedule_manager.schedule_job(try_()(self.action), *self.action_args)
            self.is_running = True
            return
        raise exceptions.AlreadyRunningException

    def stop(self):
        if self.is_running and self._job:
            self._schedule_manager.cancel_job(self._job)
            self.is_running = False
            return
        raise exceptions.UnbegunException


class Sampler(SimpleJob):
    def __init__(self, host, required_controls, steps=None):
        super().__init__(self._init_sampler)
        self._endpoint_ip_address = None
        self.is_service_running = True
        self._undone_required_controls = required_controls
        self._host = host
        self._steps = steps or {'analyze': core_models.Step(self._analyze),
                                'sample': core_models.Step(self._sample)}

    @property
    def endpoint_ip_address(self):
        if not self._endpoint_ip_address:
            raise exceptions.UnbegunException
        return self._endpoint_ip_address

    @try_()
    def _init_sampler(self, done_callback):
        logger.debug('{} - Starting sampler'.format(str(self._host.assessment_token)))
        for name, step_settings in self._steps.items():
            if not step_settings.is_completed():
                step_settings.action(name, step_settings)
                if not step_settings.is_completed():
                    return
        self.finisher()
        logger.debug('{} - Sampler is Done'.format(str(self._host.assessment_token)))
        done_callback()

    def finisher(self):
        self._host.state = constants.WAITING_FOR_APPROVAL_STATUS
        self._host.save()
        logger.debug('{} - Waiting for approval'.format(str(self._host.assessment_token)))

    @try_()
    def _analyze(self, step_name, step_settings):
        logger.debug('{} - Starting to analyze'.format(str(self._host.assessment_token)))
        response = requests.get(constants.SSL_LABS_URL + constants.ANALYZE_ROUTE,
                                {'host': self._host.url, 'startNew': 'on'})
        if response.status_code != 200:
            raise exceptions.RequestedAPIFailedException
        content = utils.decode_bit_to_json(response.content)
        if constants.ANALYZE_ENDPOINTS_FIELD not in content:
            raise exceptions.NoEndpointsException
        self._endpoint_ip_address = content[constants.ANALYZE_ENDPOINTS_FIELD][0][constants.ANALYZE_IP_ADDRESS_FIELD]
        self._process_content(content, step_name)
        step_settings.complete()
        logger.debug('{} - Done analyzing'.format(self._host.assessment_token))

    @try_()
    def _sample(self, step_name, step_settings):
        logger.debug('{} - Starting to sample'.format(str(self._host.assessment_token)))
        response = requests.get(constants.SSL_LABS_URL + constants.SSL_LABS_GET_ENDPOINT_ROUTE,
                                {'host': self._host.url, 's': self._endpoint_ip_address})
        if response.status_code != 200:
            raise exceptions.RequestedAPIFailedException
        content = utils.decode_bit_to_json(response.content)
        self._process_content(content, step_name)
        if self._is_sample_done(content):
            step_settings.complete()
            logger.debug('{} - Done Sampling'.format(str(self._host.assessment_token)))

    def _process_content(self, content, step_name):
        completed_controls = self._get_pending_controls_data_from_content(content, step_name)
        if completed_controls:
            self._register_controls_data(completed_controls)
            self._remove_controls(*completed_controls)

    def _is_sample_done(self, content):
        result = not self._undone_required_controls or \
                 (constants.SAMPLE_PROGRESS_FIELD in content
                  and content[constants.SAMPLE_PROGRESS_FIELD] == constants.SAMPLE_PROGRESS_FIELD_FULL_STATE)
        if result:
            logger.error('{} - controls not found in response {}'.format(str(self._host.assessment_token),
                                                                         [str(control.id) for control in
                                                                          self._undone_required_controls]))
        return result

    def _register_controls_data(self, controls_to_register):
        logger.debug(
            '{} - Registering controls with data - {}'.format(str(self._host.assessment_token), controls_to_register)
        )
        if settings.IDRRA_API_URL and settings.IDRRA_API_KEY and settings.IDRRA_API_SERVICE_ID:
            response = requests.post(settings.IDRRA_API_URL + constants.IDRRA_ASSESSMENT_CONTROLS_ROUTE,
                                     json={'key': settings.IDRRA_API_KEY,
                                           'service_id': settings.IDRRA_API_SERVICE_ID,
                                           'assessment_token': str(self._host.assessment_token),
                                           'controls': [{'control_id': control['id'], 'data': control['data']}
                                                        for control in controls_to_register]})
            if response.status_code != 201:
                raise exceptions.RequestedAPIFailedException
            logger.debug('{} - Done registering'.format(str(self._host.assessment_token)))
            return response

    def _get_pending_controls_data_from_content(self, content, step_name):
        return [{'id': control.id, 'data': control.field_getter(content), 'origin': control} for
                control in self._undone_required_controls
                if control.field_getter(content) is not None and control.from_step == step_name]

    def _remove_controls(self, *controls):
        [(self._undone_required_controls.remove(control['origin']), self._host.complete_control(control['id']))
         for control in controls]
        if not self._undone_required_controls:
            self._host.finish_host_sampling()
