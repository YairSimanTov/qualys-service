import threading
import time

from schedule import Scheduler, Job


class ScheduleManager(Scheduler):
    def __init__(self, interval=0):
        super().__init__()
        self.interval = interval

    def run_continuously(self):
        cease_continuous_run = threading.Event()

        class ScheduleThread(threading.Thread):
            @classmethod
            def run(cls):
                while not cease_continuous_run.is_set():
                    self.run_pending()
                    time.sleep(self.interval)

        continuous_thread = ScheduleThread()
        continuous_thread.start()
        return cease_continuous_run

    def schedule_job(self, job_func, *params):
        new_job = Job(self.interval, self).seconds
        new_job.do(job_func, *params, done_callback=self.done_callback_wrapper(new_job))
        return new_job

    def done_callback_wrapper(self, job):
        scheduler = self

        def done_callback():
            if job in scheduler.jobs:
                scheduler.cancel_job(job)

        return done_callback
