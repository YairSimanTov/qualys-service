from django.utils import timezone
from rest_framework import serializers

from api.core.core_models import Control
from . import logger, models, constants
from .core import default_schedule_manager, default_jobs_queue, jobs, core_models
from .core.utils import number_bit_getter, basic_field_getter

scheduler = default_schedule_manager
jobs_queue = default_jobs_queue

REQUIRED_CONTROLS = [Control('570de3e3-4c12-490a-b9e4-a675dd5d926d',
                             'sample',
                             basic_field_getter(['grade'])),
                     Control('2062335f-ea87-4911-a9cf-16f8fad7796e',
                             'sample',
                             basic_field_getter(['details', 'heartbleed'])),
                     Control('Acfd1ca3-2ee2-4564-bc30-650b5938c0a6',
                             'sample',
                             basic_field_getter(['details', 'vulnBeast'])),
                     Control('Bec21f96-b4e0-4758-8204-494bb9269aa8',
                             'sample',
                             basic_field_getter(['details', 'renegSupport'], number_bit_getter(1))),
                     Control('B14bb56c-e16a-4b08-bcc7-34eeb2509baa',
                             'sample',
                             basic_field_getter(['details', 'poodle'])),
                     Control('Fda8993e-4582-4bbb-a4db-08175f315589',
                             'sample',
                             basic_field_getter(['details', 'freak']))]


def set_endpoint_ip_address(host, sampler, done_callback):
    logger.debug('{} - Trying to set endpoint from sampler to host'.format(str(host.assessment_token)))
    host.endpoint_ip_address = sampler.endpoint_ip_address
    host.save()
    done_callback()


def get_undone_host_samples():
    return models.Host.objects.filter(state=constants.PENDING_STATUS).all()


def sample_hosts(*hosts):
    for host in hosts:
        unfinished_controllers = [control for control in REQUIRED_CONTROLS if
                                  control.id not in host.done_controllers]
        sampler = jobs.Sampler(host, unfinished_controllers)
        end_point_getter = jobs.SimpleJob(set_endpoint_ip_address, host, sampler)
        default_jobs_queue.enqueue(core_models.JobBundle(host, sampler, end_point_getter))


class HostSerializer(serializers.ModelSerializer):
    website = serializers.URLField(source='url')

    class Meta:
        model = models.Host
        fields = ['website', 'assessment_token']
        read_only_fields = ['id']

    def create(self, validated_data):
        return self._stop_host_process(validated_data['assessment_token']) \
               or self._create_new_host(validated_data)

    @classmethod
    def _create_new_host(cls, validated_data):
        logger.debug('{} - creating new host'.format(str(validated_data['assessment_token'])))
        validated_data['done_controllers'] = []
        validated_data['start_time'] = timezone.now()
        host = models.Host(**validated_data)
        host.save()
        sample_hosts(host)
        return host

    @classmethod
    def _stop_host_process(cls, assessment_token):
        try:
            host = models.Host.objects.get(state__in=[constants.PENDING_STATUS, constants.WAITING_FOR_APPROVAL_STATUS],
                                           assessment_token=assessment_token)
            jobs_queue.stop_running_bundles(host)
            jobs_queue.cancel_pending_bundles(host)
            host.state = constants.STOPPED_STATUS
            host.end_time = timezone.now()
            host.save()
            logger.debug('{} - host stopped'.format(str(host.assessment_token)))
            return host
        except models.Host.DoesNotExist:
            return None
