class FakeScheduler:
    def __init__(self):
        self.call_count = {'schedule_job': 0,
                           'cancel_job': 0}

    def schedule_job(self, job):
        self.call_count['schedule_job'] += 1
        return True

    def cancel_job(self, job):
        self.call_count['cancel_job'] += 1
        return True
