import requests_mock

from .stubs import request_stub, GET, POST


def request_factory(mocker, request, failure):
    def get_status_code():
        if failure:
            return 400
        elif 'status_code' in request:
            return request['status_code']
        return 200

    methods = {
        GET: mocker.get,
        POST: mocker.post
    }
    return methods[request['method']](request['url'], json=request['json'], status_code=get_status_code())


def request_mock(failure=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            with requests_mock.Mocker() as mocker:
                requests_mocks = [request_factory(mocker, request, failure) for request in request_stub]
                func(*args, **kwargs)

        return wrapper

    return decorator
