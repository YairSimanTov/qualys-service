GET = 'GET'
POST = 'POST'

request_stub = [
    {
        'method': GET,
        'url': 'https://www.a.com',
        'json': '{"a": "b"}'
    }, {
        'method': GET,
        'url': 'https://api.ssllabs.com/api/v3/analyze',
        'json': {
            "host": "app.idrra.com",
            "port": 443,
            "protocol": "http",
            "isPublic": False,
            "status": "IN_PROGRESS",
            "startTime": 1546129456520,
            "engineVersion": "1.32.13",
            "criteriaVersion": "2009p",
            "endpoints": [
                {
                    "ipAddress": "100.26.72.155",
                    "serverName": "ec2-100-26-72-155.compute-1.amazonaws.com",
                    "statusMessage": "In progress",
                    "statusDetails": "TESTING_PROTO_3_2",
                    "statusDetailsMessage": "Testing TLS 1.1",
                    "delegation": 1
                },
                {
                    "ipAddress": "52.73.185.58",
                    "serverName": "ec2-52-73-185-58.compute-1.amazonaws.com",
                    "statusMessage": "Pending",
                    "delegation": 1
                },
                {
                    "ipAddress": "34.193.89.105",
                    "serverName": "ec2-34-193-89-105.compute-1.amazonaws.com",
                    "statusMessage": "Pending",
                    "delegation": 1
                },
                {
                    "ipAddress": "18.210.76.24",
                    "serverName": "ec2-18-210-76-24.compute-1.amazonaws.com",
                    "statusMessage": "Pending",
                    "delegation": 1
                },
                {
                    "ipAddress": "54.209.184.12",
                    "serverName": "ec2-54-209-184-12.compute-1.amazonaws.com",
                    "statusMessage": "Pending",
                    "delegation": 1
                },
                {
                    "ipAddress": "54.243.14.13",
                    "serverName": "ec2-54-243-14-13.compute-1.amazonaws.com",
                    "statusMessage": "Pending",
                    "delegation": 1
                }
            ]
        }
    }, {
        'method': GET,
        'url': 'https://api.ssllabs.com/api/v3/getEndpointData',
        'json': {
            "ipAddress": "100.26.72.155",
            "serverName": "ec2-100-26-72-155.compute-1.amazonaws.com",
            "statusMessage": "Ready",
            "grade": "A+",
            "gradeTrustIgnored": "A+",
            "hasWarnings": False,
            "isExceptional": True,
            "progress": 100,
            "duration": 112067,
            "delegation": 1,
            "details": {
                "hostStartTime": 1546129456520,
                "certChains": [
                    {
                        "id": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                        "certIds": [
                            "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                            "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                            "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                            "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996"
                        ],
                        "trustPaths": [
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "8ecde6884f3d87b1125ba31ac3fcb13d7016de7f57cc904fe1cb97c6ae98196e"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Mozilla",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "568d6905a2c88708a4b3025190edcfedb1974a606a13c6e5290fcb2ae63edab5"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Mozilla",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "1465fa205397b876faa6f0a9958e5590e40fcc7faa4fb7c2c8677521fb5fb658"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Mozilla",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "d73494e3446b02167573b3cde3ae1c8584ac26e15e45ac3ec0326708425d90fb"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Mozilla",
                                        "isTrusted": False,
                                        "trustErrorMessage": "path does not chain to a trusted anchor"
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "8ecde6884f3d87b1125ba31ac3fcb13d7016de7f57cc904fe1cb97c6ae98196e"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Apple",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "568d6905a2c88708a4b3025190edcfedb1974a606a13c6e5290fcb2ae63edab5"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Apple",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "1465fa205397b876faa6f0a9958e5590e40fcc7faa4fb7c2c8677521fb5fb658"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Apple",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "d73494e3446b02167573b3cde3ae1c8584ac26e15e45ac3ec0326708425d90fb"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Apple",
                                        "isTrusted": False,
                                        "trustErrorMessage": "path does not chain to a trusted anchor"
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "8ecde6884f3d87b1125ba31ac3fcb13d7016de7f57cc904fe1cb97c6ae98196e"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Android",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "568d6905a2c88708a4b3025190edcfedb1974a606a13c6e5290fcb2ae63edab5"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Android",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "1465fa205397b876faa6f0a9958e5590e40fcc7faa4fb7c2c8677521fb5fb658"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Android",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "d73494e3446b02167573b3cde3ae1c8584ac26e15e45ac3ec0326708425d90fb"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Android",
                                        "isTrusted": False,
                                        "trustErrorMessage": "path does not chain to a trusted anchor"
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "568d6905a2c88708a4b3025190edcfedb1974a606a13c6e5290fcb2ae63edab5"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Java",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "1465fa205397b876faa6f0a9958e5590e40fcc7faa4fb7c2c8677521fb5fb658"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Java",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "d73494e3446b02167573b3cde3ae1c8584ac26e15e45ac3ec0326708425d90fb"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Java",
                                        "isTrusted": False,
                                        "trustErrorMessage": "path does not chain to a trusted anchor"
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "8ecde6884f3d87b1125ba31ac3fcb13d7016de7f57cc904fe1cb97c6ae98196e"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Windows",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "568d6905a2c88708a4b3025190edcfedb1974a606a13c6e5290fcb2ae63edab5"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Windows",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "1465fa205397b876faa6f0a9958e5590e40fcc7faa4fb7c2c8677521fb5fb658"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Windows",
                                        "isTrusted": True
                                    }
                                ]
                            },
                            {
                                "certIds": [
                                    "3e0f6a5fae749a8269ed7449ad5a6a542c303ebd924e160977de1f73f1de1968",
                                    "f55f9ffcb83c73453261601c7e044db15a0f034b93c05830f28635ef889cf670",
                                    "87dcd4dc74640a322cd205552506d1be64f12596258096544986b4850bc72706",
                                    "28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996",
                                    "d73494e3446b02167573b3cde3ae1c8584ac26e15e45ac3ec0326708425d90fb"
                                ],
                                "trust": [
                                    {
                                        "rootStore": "Windows",
                                        "isTrusted": False,
                                        "trustErrorMessage": "path does not chain to a trusted anchor"
                                    }
                                ]
                            }
                        ],
                        "issues": 0,
                        "noSni": False
                    }
                ],
                "protocols": [
                    {
                        "id": 769,
                        "name": "TLS",
                        "version": "1.0"
                    },
                    {
                        "id": 770,
                        "name": "TLS",
                        "version": "1.1"
                    },
                    {
                        "id": 771,
                        "name": "TLS",
                        "version": "1.2"
                    }
                ],
                "suites": [
                    {
                        "protocol": 769,
                        "list": [
                            {
                                "id": 49171,
                                "name": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                                "cipherStrength": 128,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49172,
                                "name": "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
                                "cipherStrength": 256,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 47,
                                "name": "TLS_RSA_WITH_AES_128_CBC_SHA",
                                "cipherStrength": 128
                            },
                            {
                                "id": 53,
                                "name": "TLS_RSA_WITH_AES_256_CBC_SHA",
                                "cipherStrength": 256
                            }
                        ],
                        "preference": True
                    },
                    {
                        "protocol": 770,
                        "list": [
                            {
                                "id": 49171,
                                "name": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                                "cipherStrength": 128,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49172,
                                "name": "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
                                "cipherStrength": 256,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 47,
                                "name": "TLS_RSA_WITH_AES_128_CBC_SHA",
                                "cipherStrength": 128
                            },
                            {
                                "id": 53,
                                "name": "TLS_RSA_WITH_AES_256_CBC_SHA",
                                "cipherStrength": 256
                            }
                        ],
                        "preference": True
                    },
                    {
                        "protocol": 771,
                        "list": [
                            {
                                "id": 49199,
                                "name": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                                "cipherStrength": 128,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49191,
                                "name": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                                "cipherStrength": 128,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49171,
                                "name": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                                "cipherStrength": 128,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49200,
                                "name": "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
                                "cipherStrength": 256,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49192,
                                "name": "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384",
                                "cipherStrength": 256,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 49172,
                                "name": "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
                                "cipherStrength": 256,
                                "kxType": "ECDH",
                                "kxStrength": 3072,
                                "namedGroupBits": 256,
                                "namedGroupId": 23,
                                "namedGroupName": "secp256r1"
                            },
                            {
                                "id": 156,
                                "name": "TLS_RSA_WITH_AES_128_GCM_SHA256",
                                "cipherStrength": 128
                            },
                            {
                                "id": 60,
                                "name": "TLS_RSA_WITH_AES_128_CBC_SHA256",
                                "cipherStrength": 128
                            },
                            {
                                "id": 47,
                                "name": "TLS_RSA_WITH_AES_128_CBC_SHA",
                                "cipherStrength": 128
                            },
                            {
                                "id": 157,
                                "name": "TLS_RSA_WITH_AES_256_GCM_SHA384",
                                "cipherStrength": 256
                            },
                            {
                                "id": 61,
                                "name": "TLS_RSA_WITH_AES_256_CBC_SHA256",
                                "cipherStrength": 256
                            },
                            {
                                "id": 53,
                                "name": "TLS_RSA_WITH_AES_256_CBC_SHA",
                                "cipherStrength": 256
                            }
                        ],
                        "preference": True
                    }
                ],
                "namedGroups": {
                    "list": [
                        {
                            "id": 23,
                            "name": "secp256r1",
                            "bits": 256,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 25,
                            "name": "secp521r1",
                            "bits": 521,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 28,
                            "name": "brainpoolP512r1",
                            "bits": 512,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 27,
                            "name": "brainpoolP384r1",
                            "bits": 384,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 24,
                            "name": "secp384r1",
                            "bits": 384,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 26,
                            "name": "brainpoolP256r1",
                            "bits": 256,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 22,
                            "name": "secp256k1",
                            "bits": 256,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 14,
                            "name": "sect571r1",
                            "bits": 571,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 13,
                            "name": "sect571k1",
                            "bits": 571,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 11,
                            "name": "sect409k1",
                            "bits": 409,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 12,
                            "name": "sect409r1",
                            "bits": 409,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 9,
                            "name": "sect283k1",
                            "bits": 283,
                            "namedGroupType": "EC"
                        },
                        {
                            "id": 10,
                            "name": "sect283r1",
                            "bits": 283,
                            "namedGroupType": "EC"
                        }
                    ],
                    "preference": True
                },
                "serverSignature": "Apache",
                "prefixDelegation": False,
                "nonPrefixDelegation": True,
                "vulnBeast": True,
                "renegSupport": 2,
                "sessionResumption": 2,
                "compressionMethods": 0,
                "supportsNpn": True,
                "npnProtocols": "h2 http/1.1",
                "supportsAlpn": True,
                "alpnProtocols": "h2 http/1.1",
                "sessionTickets": 1,
                "ocspStapling": False,
                "sniRequired": False,
                "httpStatusCode": 200,
                "supportsRc4": False,
                "rc4WithModern": False,
                "rc4Only": False,
                "forwardSecrecy": 2,
                "supportsAead": True,
                "protocolIntolerance": 0,
                "miscIntolerance": 0,
                "sims": {
                    "results": [
                        {
                            "client": {
                                "id": 56,
                                "name": "Android",
                                "version": "2.3.7",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 47,
                            "suiteName": "TLS_RSA_WITH_AES_128_CBC_SHA",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 58,
                                "name": "Android",
                                "version": "4.0.4",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 59,
                                "name": "Android",
                                "version": "4.1.1",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 60,
                                "name": "Android",
                                "version": "4.2.2",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 61,
                                "name": "Android",
                                "version": "4.3",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 62,
                                "name": "Android",
                                "version": "4.4.2",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 88,
                                "name": "Android",
                                "version": "5.0.0",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 129,
                                "name": "Android",
                                "version": "6.0",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 139,
                                "name": "Android",
                                "version": "7.0",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 94,
                                "name": "Baidu",
                                "version": "Jan 2015",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 91,
                                "name": "BingPreview",
                                "version": "Jan 2015",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 136,
                                "name": "Chrome",
                                "platform": "XP SP3",
                                "version": "49",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 152,
                                "name": "Chrome",
                                "platform": "Win 7",
                                "version": "69",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 153,
                                "name": "Chrome",
                                "platform": "Win 10",
                                "version": "70",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 84,
                                "name": "Firefox",
                                "platform": "Win 7",
                                "version": "31.3.0 ESR",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 132,
                                "name": "Firefox",
                                "platform": "Win 7",
                                "version": "47",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 137,
                                "name": "Firefox",
                                "platform": "XP SP3",
                                "version": "49",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 151,
                                "name": "Firefox",
                                "platform": "Win 7",
                                "version": "62",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 145,
                                "name": "Googlebot",
                                "version": "Feb 2018",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 100,
                                "name": "IE",
                                "platform": "XP",
                                "version": "6",
                                "isReference": False
                            },
                            "errorCode": 1,
                            "errorMessage": "Protocol mismatch (not simulated)",
                            "attempts": 0
                        },
                        {
                            "client": {
                                "id": 19,
                                "name": "IE",
                                "platform": "Vista",
                                "version": "7",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 101,
                                "name": "IE",
                                "platform": "XP",
                                "version": "8",
                                "isReference": False
                            },
                            "errorCode": 1,
                            "errorMessage": "Server sent fatal alert: handshake_failure",
                            "attempts": 1,
                            "alertType": 2,
                            "alertCode": 40
                        },
                        {
                            "client": {
                                "id": 113,
                                "name": "IE",
                                "platform": "Win 7",
                                "version": "8-10",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 143,
                                "name": "IE",
                                "platform": "Win 7",
                                "version": "11",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 134,
                                "name": "IE",
                                "platform": "Win 8.1",
                                "version": "11",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 64,
                                "name": "IE",
                                "platform": "Win Phone 8.0",
                                "version": "10",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 65,
                                "name": "IE",
                                "platform": "Win Phone 8.1",
                                "version": "11",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 106,
                                "name": "IE",
                                "platform": "Win Phone 8.1 Update",
                                "version": "11",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 131,
                                "name": "IE",
                                "platform": "Win 10",
                                "version": "11",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 144,
                                "name": "Edge",
                                "platform": "Win 10",
                                "version": "15",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 120,
                                "name": "Edge",
                                "platform": "Win Phone 10",
                                "version": "13",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 25,
                                "name": "Java",
                                "version": "6u45",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "protocolId": 769,
                            "suiteId": 47,
                            "suiteName": "TLS_RSA_WITH_AES_128_CBC_SHA",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 26,
                                "name": "Java",
                                "version": "7u25",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 147,
                                "name": "Java",
                                "version": "8u161",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 27,
                                "name": "OpenSSL",
                                "version": "0.9.8y",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 47,
                            "suiteName": "TLS_RSA_WITH_AES_128_CBC_SHA",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 99,
                                "name": "OpenSSL",
                                "version": "1.0.1l",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 121,
                                "name": "OpenSSL",
                                "version": "1.0.2e",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 32,
                                "name": "Safari",
                                "platform": "OS X 10.6.8",
                                "version": "5.1.9",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 33,
                                "name": "Safari",
                                "platform": "iOS 6.0.1",
                                "version": "6",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 34,
                                "name": "Safari",
                                "platform": "OS X 10.8.4",
                                "version": "6.0.4",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 769,
                            "suiteId": 49171,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 63,
                                "name": "Safari",
                                "platform": "iOS 7.1",
                                "version": "7",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 35,
                                "name": "Safari",
                                "platform": "OS X 10.9",
                                "version": "7",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 85,
                                "name": "Safari",
                                "platform": "iOS 8.4",
                                "version": "8",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 87,
                                "name": "Safari",
                                "platform": "OS X 10.10",
                                "version": "8",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49191,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 114,
                                "name": "Safari",
                                "platform": "iOS 9",
                                "version": "9",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 111,
                                "name": "Safari",
                                "platform": "OS X 10.11",
                                "version": "9",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 140,
                                "name": "Safari",
                                "platform": "iOS 10",
                                "version": "10",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 138,
                                "name": "Safari",
                                "platform": "OS X 10.12",
                                "version": "10",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 112,
                                "name": "Apple ATS",
                                "platform": "iOS 9",
                                "version": "9",
                                "isReference": True
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 92,
                                "name": "Yahoo Slurp",
                                "version": "Jan 2015",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        },
                        {
                            "client": {
                                "id": 93,
                                "name": "YandexBot",
                                "version": "Jan 2015",
                                "isReference": False
                            },
                            "errorCode": 0,
                            "attempts": 1,
                            "certChainId": "447ad6abf37320391fe8f7789051bbd4862cb5af103a8cc7e57f18413fc7f3c1",
                            "protocolId": 771,
                            "suiteId": 49199,
                            "suiteName": "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                            "kxType": "ECDH",
                            "kxStrength": 3072,
                            "namedGroupBits": 256,
                            "namedGroupId": 23,
                            "namedGroupName": "secp256r1",
                            "keyAlg": "RSA",
                            "keySize": 2048,
                            "sigAlg": "SHA256withRSA"
                        }
                    ]
                },
                "heartbleed": False,
                "heartbeat": False,
                "openSslCcs": 1,
                "openSSLLuckyMinus20": 1,
                "ticketbleed": 1,
                "bleichenbacher": 1,
                "poodle": False,
                "poodleTls": 1,
                "fallbackScsv": True,
                "freak": False,
                "hasSct": 1,
                "ecdhParameterReuse": False,
                "logjam": False,
                "hstsPolicy": {
                    "LONG_MAX_AGE": 15552000,
                    "header": "max-age=31536000; includeSubdomains; preload;",
                    "status": "present",
                    "maxAge": 31536000,
                    "includeSubDomains": True,
                    "preload": True,
                    "directives": {
                        "includesubdomains": "",
                        "max-age": "31536000",
                        "preload": ""
                    }
                },
                "hstsPreloads": [
                    {
                        "source": "Chrome",
                        "hostname": "app.idrra.com",
                        "status": "absent",
                        "sourceTime": 1546127521041
                    },
                    {
                        "source": "Edge",
                        "hostname": "app.idrra.com",
                        "status": "absent",
                        "sourceTime": 1546126801954
                    },
                    {
                        "source": "Firefox",
                        "hostname": "app.idrra.com",
                        "status": "absent",
                        "sourceTime": 1546126801954
                    },
                    {
                        "source": "IE",
                        "hostname": "app.idrra.com",
                        "status": "absent",
                        "sourceTime": 1546126801954
                    }
                ],
                "hpkpPolicy": {
                    "status": "absent",
                    "pins": [],
                    "matchedPins": [],
                    "directives": []
                },
                "hpkpRoPolicy": {
                    "status": "absent",
                    "pins": [],
                    "matchedPins": [],
                    "directives": []
                },
                "staticPkpPolicy": {
                    "status": "absent",
                    "pins": [],
                    "matchedPins": [],
                    "forbiddenPins": [],
                    "matchedForbiddenPins": []
                },
                "httpTransactions": [
                    {
                        "requestUrl": "https://app.idrra.com/",
                        "statusCode": 200,
                        "requestLine": "GET / HTTP/1.1",
                        "requestHeaders": [
                            "Host: app.idrra.com",
                            "User-Agent: SSL Labs (https://www.ssllabs.com/about/assessment.html); on behalf of XXX.XXX.XXX.XXX",
                            "Accept: */*",
                            "Connection: Close"
                        ],
                        "responseLine": "HTTP/1.1 200 OK",
                        "responseHeadersRaw": [
                            "Date: Sun, 30 Dec 2018 00:24:22 GMT",
                            "Content-Type: text/html; charset=utf-8",
                            "Content-Length: 3037",
                            "Connection: close",
                            "Server: Apache",
                            "Vary: Cookie,Accept-Encoding",
                            "X-Frame-Options: SAMEORIGIN, SAMEORIGIN",
                            "X-Amzn-Trace-Id: Root=1-5c281036-84ad470ad6fbda7c44d4b976",
                            "Set-Cookie: csrftoken=wbtHUPxjfszZy1moxQZMStaOKPV7KqQwQuruFUsupvfRxqEgMA7FvCNkNQwdy9aX; expires=Sun, 29 Dec 2019 00:24:22 GMT; Max-Age=31449600; Path=/; SameSite=Lax; Secure",
                            "Strict-Transport-Security: max-age=31536000; includeSubdomains; preload;",
                            "Referrer-Policy: same-origin",
                            "Feature-Policy: microphone 'none'; payment 'none';",
                            "X-XSS-Protection: 1; mode=block",
                            "X-Content-Type-Options: nosniff"
                        ],
                        "responseHeaders": [
                            {
                                "name": "Date",
                                "value": "Sun, 30 Dec 2018 00:24:22 GMT"
                            },
                            {
                                "name": "Content-Type",
                                "value": "text/html; charset=utf-8"
                            },
                            {
                                "name": "Content-Length",
                                "value": "3037"
                            },
                            {
                                "name": "Connection",
                                "value": "close"
                            },
                            {
                                "name": "Server",
                                "value": "Apache"
                            },
                            {
                                "name": "Vary",
                                "value": "Cookie,Accept-Encoding"
                            },
                            {
                                "name": "X-Frame-Options",
                                "value": "SAMEORIGIN, SAMEORIGIN"
                            },
                            {
                                "name": "X-Amzn-Trace-Id",
                                "value": "Root=1-5c281036-84ad470ad6fbda7c44d4b976"
                            },
                            {
                                "name": "Set-Cookie",
                                "value": "csrftoken=wbtHUPxjfszZy1moxQZMStaOKPV7KqQwQuruFUsupvfRxqEgMA7FvCNkNQwdy9aX; expires=Sun, 29 Dec 2019 00:24:22 GMT; Max-Age=31449600; Path=/; SameSite=Lax; Secure"
                            },
                            {
                                "name": "Strict-Transport-Security",
                                "value": "max-age=31536000; includeSubdomains; preload;"
                            },
                            {
                                "name": "Referrer-Policy",
                                "value": "same-origin"
                            },
                            {
                                "name": "Feature-Policy",
                                "value": "microphone 'none'; payment 'none';"
                            },
                            {
                                "name": "X-XSS-Protection",
                                "value": "1; mode=block"
                            },
                            {
                                "name": "X-Content-Type-Options",
                                "value": "nosniff"
                            }
                        ],
                        "fragileServer": False
                    }
                ],
                "drownHosts": [],
                "drownErrors": False,
                "drownVulnerable": False
            }
        }
    }, {
        'method': POST,
        'url': 'http://127.0.0.1:8000/AssessmentControls/',
        'json': {},
        'status_code': 201
    }


]
