import copy
from uuid import uuid4

import pytest
from django.test import TestCase
from django.utils import timezone

from . import mockers, request_mock
from .. import constants, models
from ..core import core_models, jobs, exceptions, utils


class UtilsTestCase(TestCase):
    @classmethod
    def test_basic_field_getter(cls):
        input = {
            'a': {
                'c': {
                    'b': 2
                }
            }
        }
        assert utils.basic_field_getter(['a', 'c', 'b'], value_logic=lambda x: x * x)(input) == 4


# models
class StepTestCase(TestCase):
    @classmethod
    def test_complete_step_changing_state(cls):
        step = core_models.Step(None)
        step.complete()
        assert step.state == constants.WAITING_FOR_APPROVAL_STATUS

    @classmethod
    def test_is_completed(cls):
        step = core_models.Step(None)
        assert not step.is_completed()
        step.state = constants.WAITING_FOR_APPROVAL_STATUS
        assert step.is_completed()


class QueueTestCase(TestCase):
    @classmethod
    def test_is_empty_return_if_empty(cls):
        assert core_models.Queue().is_empty()

    @classmethod
    def test_enqueue_append_item(cls):
        queue = core_models.Queue()
        queue.enqueue('a')
        assert len(queue.items)

    @classmethod
    def test_dequeue_should_return_top_item_if_exist(cls):
        queue = core_models.Queue()
        queue.items.append('a')
        assert queue.dequeue() == 'a'
        with pytest.raises(IndexError):
            queue.dequeue()

    @classmethod
    def test_remove_from_queue_removes_only_the_item(cls):
        queue = core_models.Queue()
        queue.items = ['a']
        assert queue.remove_from_queue('a') == [None]
        assert queue.remove_from_queue('b') == []

    @classmethod
    def test_size_returns_the_amount_of_items_in_queue(cls):
        queue = core_models.Queue()
        assert queue.size == 0


class JobsQueueTestCase(TestCase):
    def setUp(self):
        self.scheduler = mockers.FakeScheduler()
        self.jobs_queue = core_models.JobsQueue(self.scheduler)
        self.jobs_queue.setup_queue()
        self.bundle = core_models.JobBundle(None, jobs.SimpleJob(None, schedule_manager=self.scheduler))

    def test_setup_queue_setting_ready_to_true(self):
        self.jobs_queue._ready = False
        self.jobs_queue.setup_queue()
        assert self.jobs_queue._ready

    def test_setup_queue_is_telling_the_scheduler_to_run_the_queue_runner(self):
        self.jobs_queue.setup_queue()
        assert self.jobs_queue.scheduler.call_count['schedule_job']

    def test_stop_running_bundles_should_call_stop_for_each_job_in_running_bundle(self):
        self.jobs_queue.running_bundles.append(self.bundle)
        self.jobs_queue.stop_running_bundles(self.bundle)
        assert self.jobs_queue.scheduler.call_count['schedule_job'] == 1

    def test_stop_running_bundles_should_remove_bundles_from_running_bundles(self):
        [job.start() for job in self.bundle.jobs]
        self.jobs_queue.running_bundles.append(self.bundle)
        self.jobs_queue.stop_running_bundles(self.bundle.host)
        assert self.bundle not in self.jobs_queue.running_bundles

    def test_cancel_pending_bundles_should_remove_bundles_from_queue(self):
        self.jobs_queue.enqueue(self.bundle)
        assert self.jobs_queue._queue.items
        self.jobs_queue.cancel_pending_bundles(self.bundle.host)
        assert not self.jobs_queue._queue.items

    def test_remove_completed_bundles_should_remove_the_bundles_with_all_jobs_not_running(self):
        self.jobs_queue.running_bundles.append(self.bundle)
        self.jobs_queue.remove_completed_bundles()
        assert not self.jobs_queue.running_bundles

    def test_queue_runner_should_remove_the_bundles_with_all_jobs_not_running(self):
        self.jobs_queue.is_running = False
        self.jobs_queue.running_bundles.append(self.bundle)
        self.jobs_queue._queue_runner(None)
        assert not self.jobs_queue.running_bundles

    @pytest.mark.timeout(5)
    def test_queue_runner_should_run_pending_bundles_until_queue_is_full_or_there_are_no_pending_bundles(self):
        self.jobs_queue.is_running = False
        [self.jobs_queue.enqueue(copy.deepcopy(self.bundle)) for i in range(5)]
        self.jobs_queue._queue_runner(None)
        assert len(self.jobs_queue.running_bundles) == 5
        self.jobs_queue.is_running = False
        [self.jobs_queue.enqueue(copy.deepcopy(self.bundle)) for i in range(6)]
        self.jobs_queue._queue_runner(None)
        assert len(self.jobs_queue.running_bundles) == constants.MAX_RUNNING_JOBS


# jobs
def test_try_decorator_should_not_raise_exception_on_allowed_exception_raise():
    def func():
        raise exceptions.UnbegunException

    jobs.try_()(func)()


def test_try_decorator_should_raise_exception_if_the_exception_is_no_allowed():
    def func():
        raise IndexError

    with pytest.raises(IndexError):
        jobs.try_(True)(func)()


class SimpleJobTestCase(TestCase):
    def setUp(self):
        self.scheduler = mockers.FakeScheduler()
        self.job = jobs.SimpleJob(None, schedule_manager=self.scheduler)

    def test_start_should_raise_exception_if_the_job_already_started(self):
        self.job.start()
        with pytest.raises(exceptions.AlreadyRunningException):
            self.job.start()

    def test_start_should_schedule_job(self):
        self.job.start()
        assert self.scheduler.call_count['schedule_job']

    def test_start_should_set_running_to_true(self):
        self.job.start()
        assert self.job.is_running

    def test_stop_should_raise_exception_if_the_job_is_not_running(self):
        with pytest.raises(exceptions.UnbegunException):
            self.job.stop()

    def test_stop_should_cancel_job_from_scheduler(self):
        self.job.is_running = True
        self.job._job = True
        self.job.stop()
        assert self.scheduler.call_count['cancel_job']

    def test_stop_should_set_job_to_not_running(self):
        self.job.is_running = True
        self.job._job = True
        self.job.stop()
        assert not self.job.is_running


class SamplerTestCase(TestCase):
    def setUp(self):
        self.scheduler = mockers.FakeScheduler()
        self.host = models.Host(id=uuid4(), assessment_token=uuid4(), url='http://www.google.com',
                                start_time=timezone.now())
        self.sampler = jobs.Sampler(self.host, [])
        self.steps = {'analyze': core_models.Step(self.sampler._analyze),
                      'sample': core_models.Step(self.sampler._sample)}
        self.sampler._schedule_manager = self.scheduler

    def test_end_point_address_getter_should_raise_exception_if_false(self):
        with pytest.raises(exceptions.UnbegunException):
            x = self.sampler.endpoint_ip_address

    def test_end_point_address_getter_should_return_endpoint_if_true(self):
        self.sampler._endpoint_ip_address = True
        x = self.sampler.endpoint_ip_address

    def test_init_sampler_should_call_done_callback_and_set_is_running_to_false_if_all_the_steps_are_done(self):
        self.is_done = False

        def set_done(instance):
            def wrapper():
                instance.is_done = True

            return wrapper

        self.sampler._steps = {}
        self.sampler._init_sampler(set_done(self))
        assert self.is_done
        assert not self.sampler.is_running

    def test_init_sampler_should_return_and_not_call_done_callback_if_one_of_the_steps_is_not_done(self):
        self.is_done = False

        def set_done(instance):
            def wrapper():
                instance.is_done = True

            return wrapper

        def raise_(*args):
            raise exceptions.UnbegunException

        self.sampler._steps = {'a': core_models.Step(raise_)}
        self.sampler._init_sampler(set_done(self))
        assert not self.is_done

    @request_mock()
    def test_analyze_should_set_step_to_be_completed_on_success(self):
        self.sampler._analyze('analyze', self.steps['analyze'])
        assert self.steps['analyze'].state == constants.WAITING_FOR_APPROVAL_STATUS

    @request_mock(failure=True)
    def test_analyze_should_not_set_step_to_be_completed_on_failure(self):
        self.sampler._analyze('analyze', {self.steps['analyze']})
        assert self.steps['analyze'] != constants.PENDING_STATUS

    @request_mock()
    def test_sample_should_set_step_to_be_completed_on_success(self):
        self.sampler._analyze('sample', self.steps['sample'])
        assert self.steps['sample'].state == constants.WAITING_FOR_APPROVAL_STATUS

    @request_mock(failure=True)
    def test_sample_should_not_set_step_to_be_completed_on_failure(self):
        self.sampler._analyze('sample', {self.steps['sample']})
        assert self.steps['sample'] != constants.PENDING_STATUS

    def test_is_sample_done_should_return_true_if_there_is_no_undone_controls_or_progress_is_100_percents(self):
        assert self.sampler._is_sample_done({})
        self.sampler._undone_required_controls = [core_models.Control(uuid4(), 'sample', utils.basic_field_getter('s'))]
        assert self.sampler._is_sample_done(
            {constants.SAMPLE_PROGRESS_FIELD: constants.SAMPLE_PROGRESS_FIELD_FULL_STATE})
        assert not self.sampler._is_sample_done({})

    @request_mock()
    def test_register_controls_data_should_send_controls_data_to_idrra_api(self):
        controls = [{'id': 'a', 'data': '{"c": "b"}'}]
        response = self.sampler._register_controls_data(controls)
        assert response.status_code == 201

    @request_mock(failure=True)
    def test_on_register_failure_should_raise_exception(self):
        controls = [{'id': 'a', 'data': '{"c": "b"}'}]
        with pytest.raises(exceptions.RequestedAPIFailedException):
            self.sampler._register_controls_data(controls)

    def test_get_pending_controls_data_from_content_should_return_all_undone_controls_in_the_content(self):
        self.sampler._undone_required_controls = [
            core_models.Control('123', 'sample', utils.basic_field_getter('a'))
        ]
        expected = [{
            'id': '123',
            'data': 'b',
            'origin': self.sampler._undone_required_controls[0]
        }]
        result = self.sampler._get_pending_controls_data_from_content({'a': 'b'}, 'sample')
        assert result == expected

    @request_mock()
    def test_remove_controls_should_remove_input_controls_from_undone(self):
        controls = [
            {'id': uuid4(), 'origin': core_models.Control('123', 'sample', utils.basic_field_getter('a'))}
        ]
        self.sampler._undone_required_controls = [control['origin'] for control in controls]
        self.sampler._remove_controls(*controls)
        assert not self.sampler._undone_required_controls

    @request_mock()
    def test_remove_controls_should_set_host_state_to_done_status(self):
        controls = [
            {'id': uuid4(), 'origin': core_models.Control(uuid4(), 'sample', utils.basic_field_getter('a'))}
        ]
        self.sampler._undone_required_controls = [control['origin'] for control in controls]
        self.sampler._remove_controls(*controls)
        self.host.state = constants.WAITING_FOR_APPROVAL_STATUS

    @request_mock()
    def test_remove_controls_should_set_completed_controls(self):
        controls = [
            {'id': uuid4(), 'origin': core_models.Control('123', 'sample', utils.basic_field_getter('a'))}
        ]
        self.sampler._undone_required_controls = [control['origin'] for control in controls]
        self.sampler._remove_controls(*controls)
        assert len(self.host.done_controllers)
