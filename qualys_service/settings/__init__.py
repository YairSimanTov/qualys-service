import os

from . import local, production, testing, dev

configuration_dict = {
    'local': 'settings.local',
    'dev': 'settings.dev',
    'prod': 'settings.production',
    'test': 'settings.testing'
}


def get_configuration():
    env = os.environ.get('ENV')
    if env in configuration_dict:
        return configuration_dict[env]
    return configuration_dict['dev']
